# Data Analysis
## MLD
![MLD](IMAGE/MLD.png)




## Database
- There is a database named "world".

## Tables
### Table: CITY
- Table `city` has 5 columns: `ID`, `Name`, `CountryCode`, `District`, and `Population`.
    - `ID` column can store only integer values. It is the primary key of the `city` table, cannot be null, and auto-increments for each new row.
    - `Name` column is for registering the name of the cities with a maximum length of 35 characters. It cannot be null and has a default value of an empty string.
    - `CountryCode` column is for registering the shortform name code of the country with a maximum length of 3 characters. It cannot be null and has a default value of an empty string.
    - `District` column is for registering district names with a maximum length of 20 characters. It cannot be null and has a default value of an empty string.
    - `Population` column is for registering population data. It cannot be null and has a default value of 0.
    - **Primary Key:**
        - `PRIMARY KEY (ID)`: This designates the `ID` column as the primary key for the table, uniquely identifying each row.
    - **Index:**
        - `KEY CountryCode (CountryCode)`: This creates an index on the `CountryCode` column, optimizing search queries.
    - **Foreign Key Constraint:**
        - `CONSTRAINT city_ibfk_1 FOREIGN KEY (CountryCode) REFERENCES country (Code)`: This establishes a foreign key constraint, ensuring referential integrity between the `city` and `country` tables.
    - **Storage Engine and Character Set:**
        - `ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;`: Specifies that the storage engine for the table is InnoDB, supporting features like transactions and foreign keys. The default character set is utf8mb4.

### Table: COUNTRY
- Table `country` has 15 columns: `Code`, `Name`, `Continent`, `Region`, `SurfaceArea`, `IndepYear`, `Population`, `LifeExpectancy`, `GNP`, `GNPOld`, `LocalName`, `GovernmentForm`, `HeadOfState`, `Capital`, `Code2`.
    - `Code` column is for registering a three-character country code. It cannot be null and has a default value of an empty string.
    - `Name` column is for registering the name of the country with a maximum length of 52 characters. It cannot be null and has a default value of an empty string.
    - `Continent` column is an enumeration with values: 'Asia', 'Europe', 'North America', 'Africa', 'Oceania', 'Antarctica', 'South America'. It cannot be null and has a default value of 'Asia'.
    - `Region` column is for registering the region name with a maximum length of 26 characters. It cannot be null and has a default value of an empty string.
    - `SurfaceArea` column is for registering the surface area of the country as a decimal with a precision of 10 digits (2 decimal places). It cannot be null and has a default value of 0.00.
    - `IndepYear` column is for registering the year of independence as a small integer. It allows null values.
    - `Population` column is for registering population data. It cannot be null and has a default value of 0.
    - `LifeExpectancy` column is for registering life expectancy as a decimal with a precision of 3 digits (1 decimal place). It allows null values.
    - `GNP` column is for registering the Gross National Product as a decimal with a precision of 10 digits (2 decimal places). It allows null values.
    - `GNPOld` column is for registering the old Gross National Product as a decimal with a precision of 10 digits (2 decimal places). It allows null values.
    - `LocalName` column is for registering the local name of the country with a maximum length of 45 characters. It cannot be null and has a default value of an empty string.
    - `GovernmentForm` column is for registering the form of government with a maximum length of 45 characters. It cannot be null and has a default value of an empty string.
    - `HeadOfState` column is for registering the head of state with a maximum length of 60 characters. It allows null values.
    - `Capital` column is for registering the capital city as an integer. It allows null values.
    - `Code2` column is for registering a two-character country code. It cannot be null and has a default value of an empty string.
    - **Primary Key:**
        - `PRIMARY KEY (Code)`: This designates the `Code` column as the primary key for the table, uniquely identifying each row.
    - **Storage Engine and Character Set:**
        - `ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;`: Specifies that the storage engine for the table is InnoDB, supporting features like transactions and foreign keys. The default character set is utf8mb4.

### Table: COUNTRYLANGUAGE
- Table `countrylanguage` has 4 columns: `CountryCode`, `Language`, `IsOfficial`, `Percentage`.
    - `CountryCode` column is for registering a three-character country code. It cannot be null and has a default value of an empty string.
    - `Language` column is for registering the language with a maximum length of 30 characters. It cannot be null and has a default value of an empty string.
    - `IsOfficial` column is an enumeration with values: 'T' (True) or 'F' (False). It cannot be null and has a default value of 'F'.
    - `Percentage` column is for registering the percentage of speakers as a decimal with a precision of 4 digits (1 decimal place). It cannot be null and has a default value of 0.0.
    - **Primary Key:**
        - `PRIMARY KEY (CountryCode, Language)`: This designates the combination of `CountryCode` and `Language` columns as the primary key for the table, ensuring uniqueness.
    - **Index:**
        - `KEY CountryCode (CountryCode)`: This creates an index on the `CountryCode` column, optimizing search queries.
    - **Foreign Key Constraint:**
        - `CONSTRAINT countryLanguage_ibfk_1 FOREIGN KEY (CountryCode) REFERENCES country (Code)`: This establishes a foreign key constraint, ensuring referential integrity between the `countrylanguage` and `country` tables.
    - **Storage Engine and Character Set:**
        - `ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;`: Specifies that the storage engine for the table is InnoDB, supporting features like transactions and foreign keys. The default character set is utf8mb4.


# Analysis and Queries on World Database

## 1. Types of Government in Countries
List the types of government with the number of countries for each.
```SQL
SELECT GovernmentForm, COUNT(*) AS NumberOfCountries
FROM country
GROUP BY GovernmentForm
ORDER BY NumberOfCountries DESC;
```
| GovernmentForm                               | NumberOfCountries |
|----------------------------------------------|-------------------|
| Republic                                     |               123 |
| Constitutional Monarchy                      |                29 |
| Federal Republic                             |                14 |
| Dependent Territory of the UK                |                12 |
| Monarchy                                     |                 5 |
| Overseas Department of France                |                 4 |
| Nonmetropolitan Territory of France          |                 4 |
| Constitutional Monarchy, Federation          |                 4 |
| Territory of Australia                       |                 4 |
| Socialistic Republic                         |                 3 |
| Nonmetropolitan Territory of New Zealand     |                 3 |
| US Territory                                 |                 3 |
| Commonwealth of the US                       |                 2 |
| Territorial Collectivity of France           |                 2 |
| Nonmetropolitan Territory of The Netherlands |                 2 |
| Dependent Territory of Norway                |                 2 |
| Monarchy (Sultanate)                         |                 2 |
| Part of Denmark                              |                 2 |
| Special Administrative Region of China       |                 2 |
| Islamic Republic                             |                 2 |
| Federation                                   |                 1 |
| Socialistic State                            |                 1 |
| Autonomous Area                              |                 1 |
| Administrated by the UN                      |                 1 |
| Dependent Territory of the US                |                 1 |
| Independent Church State                     |                 1 |
| Parlementary Monarchy                        |                 1 |
| Constitutional Monarchy (Emirate)            |                 1 |
| Occupied by Marocco                          |                 1 |
| People'sRepublic                             |                 1 |
| Monarchy (Emirate)                           |                 1 |
| Co-administrated                             |                 1 |
| Emirate Federation                           |                 1 |
| Parliamentary Coprincipality                 |                 1 |
| Islamic Emirate                              |                 1 |

## 2. Data Modeling Choice
Why does `countrylanguage.IsOfficial` use an enum instead of a boolean?
- I think in that time boolean wasn't exist.


## 3. English Speakers Worldwide
According to the database, how many people in the world speak English?

```SQL
SELECT SUM(country.Population * countrylanguage.Percentage / 100) AS TotalEnglishSpeakers
FROM countrylanguage 
JOIN country ON country.Code = countrylanguage.CountryCode 
WHERE countrylanguage.Language like 'English';
```

| TotalEnglishSpeakers |
|----------------------|
|      347077867.30000 |

## 4. Ranking of Languages by Speakers
Provide a list of languages with the number of speakers, ranked from most spoken to least spoken.
```SQL
SELECT countrylanguage.Language, SUM(country.Population * countrylanguage.Percentage / 100) AS TotalSpeakers FROM countrylanguage JOIN country ON country.Code = countrylanguage.CountryCode GROUP BY countrylanguage.Language ORDER BY TotalSpeakers DESC LIMIT 25;
```

| Language  | TotalSpeakers |
|-----------|---------------|
| Chinese    | 1191843539.00000 |
| Hindi      |  405633070.00000 |
| Spanish    |  355029462.00000 |
| English    |  347077867.30000 |
| Arabic     |  233839238.70000 |
| Bengali    |  209304719.00000 |
| Portuguese |  177595269.40000 |
| Russian    |  160807561.30000 |
| Japanese   |  126814108.00000 |
| Punjabi    |  104025371.00000 |
| German     |   92133584.70000 |
| Javanese   |   83570158.00000 |
| Telugu     |   79065636.00000 |
| Marathi    |   75019094.00000 |
| Korean     |   72291372.00000 |
| Vietnamese |   70616218.00000 |
| French     |   69980880.40000 |
| Tamil      |   68691536.00000 |
| Urdu       |   63589470.00000 |
| Turkish    |   62205657.20000 |
| Italian    |   59864483.20000 |
| Gujarati   |   48655776.00000 |
| Malay      |   41517994.00000 |
| Kannada    |   39532818.00000 |
| Polish     |   39525035.10000 |


## 5. Measurement Unit for Country Surface Area
In what unit is the surface area of countries expressed?
- km²


## 6. Countries with Population Over 10 Million
List countries with a population exceeding 10 million, along with their capital and the percentage of the population residing in the capital.
```SQL
SELECT country.Name AS Country, country.Population AS TotalPopulation, city.Name AS Capital, (city.Population / country.Population) * 100 AS PercentageInCapital
FROM country
JOIN city ON country.Capital = city.ID
WHERE country.Population > 10000000
ORDER BY country.Population DESC;
```
| Country                               | TotalPopulation | Capital             | PercentageInCapital |
|---------------------------------------|-----------------|---------------------|---------------------|
| China                                 |      1277558000 | Peking              |              0.5849 |
| India                                 |      1013662000 | New Delhi           |              0.0297 |
| United States                         |       278357000 | Washington          |              0.2055 |
| Indonesia                             |       212107000 | Jakarta             |              4.5283 |
| Brazil                                |       170115000 | Brasília            |              1.1580 |
| Pakistan                              |       156483000 | Islamabad           |              0.3352 |
| Russian Federation                    |       146934000 | Moscow              |              5.7095 |
| Bangladesh                            |       129155000 | Dhaka               |              2.7973 |
| Japan                                 |       126714000 | Tokyo               |              6.2978 |
| Nigeria                               |       111506000 | Abuja               |              0.3140 |
| Mexico                                |        98881000 | Ciudad de México    |              8.6885 |
| Germany                               |        82164700 | Berlin              |              4.1218 |
| Vietnam                               |        79832000 | Hanoi               |              1.7662 |
| Philippines                           |        75967000 | Manila              |              2.0813 |
| Egypt                                 |        68470000 | Cairo               |              9.9160 |
| Iran                                  |        67702000 | Teheran             |              9.9832 |
| Turkey                                |        66591000 | Ankara              |              4.5624 |
| Ethiopia                              |        62565000 | Addis Abeba         |              3.9879 |
| Thailand                              |        61399000 | Bangkok             |             10.2936 |
| United Kingdom                        |        59623400 | London              |             12.2184 |
| France                                |        59225700 | Paris               |              3.5884 |
| Italy                                 |        57680000 | Roma                |              4.5832 |
| Congo, The Democratic Republic of the |        51654000 | Kinshasa            |              9.8037 |
| Ukraine                               |        50456000 | Kyiv                |              5.2006 |
| South Korea                           |        46844000 | Seoul               |             21.3082 |
| Myanmar                               |        45611000 | Rangoon (Yangon)    |              7.3704 |
| Colombia                              |        42321000 | Santafé de Bogotá   |             14.7937 |
| South Africa                          |        40377000 | Pretoria            |              1.6312 |
| Spain                                 |        39441700 | Madrid              |              7.2995 |
| Poland                                |        38653600 | Warszawa            |              4.1791 |
| Argentina                             |        37032000 | Buenos Aires        |              8.0529 |
| Tanzania                              |        33517000 | Dodoma              |              0.5639 |
| Algeria                               |        31471000 | Alger               |              6.8889 |
| Canada                                |        31147000 | Ottawa              |              1.0764 |
| Kenya                                 |        30080000 | Nairobi             |              7.6130 |
| Sudan                                 |        29490000 | Khartum             |              3.2129 |
| Morocco                               |        28351000 | Rabat               |              2.1991 |
| Peru                                  |        25662000 | Lima                |             25.1917 |
| Uzbekistan                            |        24318000 | Toskent             |              8.7075 |
| Venezuela                             |        24170000 | Caracas             |              8.1725 |
| North Korea                           |        24039000 | Pyongyang           |             10.3332 |
| Nepal                                 |        23930000 | Kathmandu           |              2.4732 |
| Iraq                                  |        23115000 | Baghdad             |             18.7584 |
| Afghanistan                           |        22720000 | Kabul               |              7.8345 |
| Romania                               |        22455500 | Bucuresti           |              8.9783 |
| Taiwan                                |        22256000 | Taipei              |             11.8679 |
| Malaysia                              |        22244000 | Kuala Lumpur        |              5.8332 |
| Uganda                                |        21778000 | Kampala             |              4.0904 |
| Saudi Arabia                          |        21607000 | Riyadh              |             15.3839 |
| Ghana                                 |        20212000 | Accra               |              5.2939 |
| Mozambique                            |        19680000 | Maputo              |              5.1775 |
| Australia                             |        18886000 | Canberra            |              1.7088 |
| Sri Lanka                             |        18827000 | Colombo             |              3.4259 |
| Yemen                                 |        18112000 | Sanaa               |              2.7805 |
| Kazakstan                             |        16223000 | Astana              |              1.9183 |
| Syria                                 |        16125000 | Damascus            |              8.3535 |
| Madagascar                            |        15942000 | Antananarivo        |              4.2383 |
| Netherlands                           |        15864000 | Amsterdam           |              4.6092 |
| Chile                                 |        15211000 | Santiago de Chile   |             30.9247 |
| Cameroon                              |        15085000 | Yaoundé             |              9.1004 |
| Côte d’Ivoire                         |        14786000 | Yamoussoukro        |              0.8792 |
| Angola                                |        12878000 | Luanda              |             15.7012 |
| Ecuador                               |        12646000 | Quito               |             12.4423 |
| Burkina Faso                          |        11937000 | Ouagadougou         |              6.9029 |
| Zimbabwe                              |        11669000 | Harare              |             12.0833 |
| Guatemala                             |        11385000 | Ciudad de Guatemala |              7.2315 |
| Mali                                  |        11234000 | Bamako              |              7.2063 |
| Cuba                                  |        11201000 | La Habana           |             20.1411 |
| Cambodia                              |        11168000 | Phnom Penh          |              5.1053 |
| Malawi                                |        10925000 | Lilongwe            |              3.9905 |
| Niger                                 |        10730000 | Niamey              |              3.9143 |
| Yugoslavia                            |        10640000 | Beograd             |             11.3158 |
| Greece                                |        10545700 | Athenai             |              7.3212 |
| Czech Republic                        |        10278100 | Praha               |             11.4917 |
| Belgium                               |        10239000 | Bruxelles [Brussel] |              1.3073 |
| Belarus                               |        10236000 | Minsk               |             16.3540 |
| Somalia                               |        10097000 | Mogadishu           |              9.8742 |
| Hungary                               |        10043200 | Budapest            |             18.0376 |

## 7. Top 10 Countries with Highest Growth Rate
List the top 10 countries with the highest growth rate between year n and n-1, including the percentage of growth.
```SQL
SELECT country.Name AS Country, (country.GNP - country.GNPOld) / country.GNPOld * 100 AS GrowthRate
FROM country
WHERE country.GNPOld IS NOT NULL
ORDER BY GrowthRate DESC
LIMIT 10;
```


| Country                               | GrowthRate |
|---------------------------------------|------------|
| Congo, The Democratic Republic of the | 181.487470 |
| Turkmenistan                          | 119.850000 |
| Tajikistan                            |  88.446970 |
| Estonia                               |  58.053990 |
| Albania                               |  28.200000 |
| Suriname                              |  23.229462 |
| Iran                                  |  22.225899 |
| Bulgaria                              |  19.756122 |
| Honduras                              |  13.540558 |
| Latvia                                |  13.459833 |


## 8. Multilingual Countries
List countries that are multilingual, indicating the number of spoken languages for each.

```SQL
SELECT country.Name AS Country, COUNT(countrylanguage.Language) AS SpokenLanguages 
FROM country 
JOIN countrylanguage ON countrylanguage.CountryCode = country.Code 
GROUP BY country.Code, country.Name 
HAVING SpokenLanguages >= 2 
ORDER BY SpokenLanguages DESC;
```

| Country                               | SpokenLanguages |
|---------------------------------------|-----------------|
| Canada                                |              12 |
| China                                 |              12 |
| India                                 |              12 |
| Russian Federation                    |              12 |
| United States                         |              12 |
| Tanzania                              |              11 |
| South Africa                          |              11 |
| Congo, The Democratic Republic of the |              10 |
| Iran                                  |              10 |
| Kenya                                 |              10 |
| Mozambique                            |              10 |
| Nigeria                               |              10 |
| Philippines                           |              10 |
| Sudan                                 |              10 |
| Uganda                                |              10 |
| Angola                                |               9 |
| Indonesia                             |               9 |
| Vietnam                               |               9 |
| Australia                             |               8 |
| Austria                               |               8 |
| Cameroon                              |               8 |
| Czech Republic                        |               8 |
| Italy                                 |               8 |
| Liberia                               |               8 |
| Myanmar                               |               8 |
| Namibia                               |               8 |
| Pakistan                              |               8 |
| Sierra Leone                          |               8 |
| Chad                                  |               8 |
| Togo                                  |               8 |
| Benin                                 |               7 |
| Bangladesh                            |               7 |
| Denmark                               |               7 |
| Ethiopia                              |               7 |
| Guinea                                |               7 |
| Kyrgyzstan                            |               7 |
| Nepal                                 |               7 |
| Ukraine                               |               7 |
| Belgium                               |               6 |
| Burkina Faso                          |               6 |
| Central African Republic              |               6 |
| Congo                                 |               6 |
| Germany                               |               6 |
| Eritrea                               |               6 |
| France                                |               6 |
| Micronesia, Federated States of       |               6 |
| Georgia                               |               6 |
| Ghana                                 |               6 |
| Guinea-Bissau                         |               6 |
| Hungary                               |               6 |
| Japan                                 |               6 |
| Kazakstan                             |               6 |
| Latvia                                |               6 |
| Mexico                                |               6 |
| Mali                                  |               6 |
| Mongolia                              |               6 |
| Northern Mariana Islands              |               6 |
| Mauritania                            |               6 |
| Mauritius                             |               6 |
| Malaysia                              |               6 |
| Panama                                |               6 |
| Romania                               |               6 |
| Senegal                               |               6 |
| Sweden                                |               6 |
| Thailand                              |               6 |
| Taiwan                                |               6 |
| Uzbekistan                            |               6 |
| Yugoslavia                            |               6 |
| Zambia                                |               6 |
| Afghanistan                           |               5 |
| Brazil                                |               5 |
| Botswana                              |               5 |
| Côte d’Ivoire                         |               5 |
| Colombia                              |               5 |
| Comoros                               |               5 |
| Estonia                               |               5 |
| Finland                               |               5 |
| Gambia                                |               5 |
| Guatemala                             |               5 |
| Guam                                  |               5 |
| Hong Kong                             |               5 |
| Iraq                                  |               5 |
| Lithuania                             |               5 |
| Luxembourg                            |               5 |
| Moldova                               |               5 |
| Macedonia                             |               5 |
| Niger                                 |               5 |
| Norway                                |               5 |
| Nauru                                 |               5 |
| Réunion                               |               5 |
| Slovakia                              |               5 |
| Aruba                                 |               4 |
| Andorra                               |               4 |
| Azerbaijan                            |               4 |
| Bulgaria                              |               4 |
| Belarus                               |               4 |
| Belize                                |               4 |
| Bolivia                               |               4 |
| Brunei                                |               4 |
| Switzerland                           |               4 |
| Chile                                 |               4 |
| Costa Rica                            |               4 |
| Spain                                 |               4 |
| Gabon                                 |               4 |
| Honduras                              |               4 |
| Cambodia                              |               4 |
| Laos                                  |               4 |
| Macao                                 |               4 |
| Monaco                                |               4 |
| Malawi                                |               4 |
| Nicaragua                             |               4 |
| Netherlands                           |               4 |
| Palau                                 |               4 |
| Poland                                |               4 |
| Paraguay                              |               4 |
| Turkmenistan                          |               4 |
| Zimbabwe                              |               4 |
| Albania                               |               3 |
| Netherlands Antilles                  |               3 |
| Argentina                             |               3 |
| American Samoa                        |               3 |
| Burundi                               |               3 |
| Bhutan                                |               3 |
| Djibouti                              |               3 |
| United Kingdom                        |               3 |
| Guyana                                |               3 |
| Israel                                |               3 |
| Jordan                                |               3 |
| Lebanon                               |               3 |
| Liechtenstein                         |               3 |
| Sri Lanka                             |               3 |
| Lesotho                               |               3 |
| Mayotte                               |               3 |
| New Caledonia                         |               3 |
| Peru                                  |               3 |
| French Polynesia                      |               3 |
| Singapore                             |               3 |
| Solomon Islands                       |               3 |
| Slovenia                              |               3 |
| Seychelles                            |               3 |
| Tajikistan                            |               3 |
| Trinidad and Tobago                   |               3 |
| Tunisia                               |               3 |
| Turkey                                |               3 |
| Tuvalu                                |               3 |
| Venezuela                             |               3 |
| Virgin Islands, U.S.                  |               3 |
| Vanuatu                               |               3 |
| Samoa                                 |               3 |
| United Arab Emirates                  |               2 |
| Armenia                               |               2 |
| Antigua and Barbuda                   |               2 |
| Bahrain                               |               2 |
| Bahamas                               |               2 |
| Barbados                              |               2 |
| Cocos (Keeling) Islands               |               2 |
| Cook Islands                          |               2 |
| Cape Verde                            |               2 |
| Christmas Island                      |               2 |
| Cyprus                                |               2 |
| Dominica                              |               2 |
| Dominican Republic                    |               2 |
| Algeria                               |               2 |
| Ecuador                               |               2 |
| Egypt                                 |               2 |
| Fiji Islands                          |               2 |
| Faroe Islands                         |               2 |
| Gibraltar                             |               2 |
| Guadeloupe                            |               2 |
| Equatorial Guinea                     |               2 |
| Greece                                |               2 |
| Greenland                             |               2 |
| French Guiana                         |               2 |
| Croatia                               |               2 |
| Haiti                                 |               2 |
| Ireland                               |               2 |
| Iceland                               |               2 |
| Jamaica                               |               2 |
| Kiribati                              |               2 |
| Saint Kitts and Nevis                 |               2 |
| South Korea                           |               2 |
| Kuwait                                |               2 |
| Libyan Arab Jamahiriya                |               2 |
| Saint Lucia                           |               2 |
| Morocco                               |               2 |
| Madagascar                            |               2 |
| Maldives                              |               2 |
| Marshall Islands                      |               2 |
| Malta                                 |               2 |
| Martinique                            |               2 |
| Niue                                  |               2 |
| New Zealand                           |               2 |
| Oman                                  |               2 |
| Papua New Guinea                      |               2 |
| Puerto Rico                           |               2 |
| North Korea                           |               2 |
| Palestine                             |               2 |
| Qatar                                 |               2 |
| Rwanda                                |               2 |
| Svalbard and Jan Mayen                |               2 |
| El Salvador                           |               2 |
| Somalia                               |               2 |
| Sao Tome and Principe                 |               2 |
| Suriname                              |               2 |
| Swaziland                             |               2 |
| Syria                                 |               2 |
| Tokelau                               |               2 |
| East Timor                            |               2 |
| Tonga                                 |               2 |
| Saint Vincent and the Grenadines      |               2 |
| Wallis and Futuna                     |               2 |
| Yemen                                 |               2 |

## 9. Countries with Multiple Official Languages
Provide a list of countries with multiple official languages, specifying the number of official languages and the total number of languages spoken in the country.
```SQL
SELECT country.Name AS Country, COUNT(DISTINCT countrylanguage.Language) AS OfficialLanguages
FROM country
JOIN countrylanguage ON countrylanguage.CountryCode = country.Code
WHERE countrylanguage.IsOfficial = 'T'
GROUP BY country.Code, country.Name
HAVING OfficialLanguages >= 2
ORDER BY OfficialLanguages DESC;
```
| Country              | OfficialLanguages |
|----------------------|-------------------|
| South Africa         |                 4 |
| Switzerland          |                 4 |
| Luxembourg           |                 3 |
| Vanuatu              |                 3 |
| Singapore            |                 3 |
| Peru                 |                 3 |
| Bolivia              |                 3 |
| Belgium              |                 3 |
| Somalia              |                 2 |
| Nauru                |                 2 |
| Palau                |                 2 |
| Paraguay             |                 2 |
| Romania              |                 2 |
| Rwanda               |                 2 |
| Burundi              |                 2 |
| Malta                |                 2 |
| Seychelles           |                 2 |
| Togo                 |                 2 |
| Tonga                |                 2 |
| Tuvalu               |                 2 |
| American Samoa       |                 2 |
| Samoa                |                 2 |
| Netherlands Antilles |                 2 |
| Marshall Islands     |                 2 |
| Madagascar           |                 2 |
| Afghanistan          |                 2 |
| Lesotho              |                 2 |
| Sri Lanka            |                 2 |
| Kyrgyzstan           |                 2 |
| Israel               |                 2 |
| Ireland              |                 2 |
| Guam                 |                 2 |
| Greenland            |                 2 |
| Faroe Islands        |                 2 |
| Finland              |                 2 |
| Cyprus               |                 2 |
| Belarus              |                 2 |
| Canada               |                 2 |



## 10. Languages Spoken in France with Percentages
List the languages spoken in France along with the percentage of speakers for each.
```SQL
SELECT countrylanguage.Language, countrylanguage.Percentage, country.Population * (countrylanguage.Percentage / 100) AS Speakers
FROM countrylanguage
JOIN country ON countrylanguage.CountryCode = country.Code
WHERE country.Name = 'France'
ORDER BY countrylanguage.Percentage DESC;
```
| Language   | Percentage | Speakers       |
|------------|------------|----------------|
| French     |       93.6 | 55435255.20000 |
| Arabic     |        2.5 |  1480642.50000 |
| Portuguese |        1.2 |   710708.40000 |
| Italian    |        0.4 |   236902.80000 |
| Spanish    |        0.4 |   236902.80000 |
| Turkish    |        0.4 |   236902.80000 |


## 11. Languages Spoken in China with Percentages
Provide a similar list for languages spoken in China, including the percentage of speakers.
```SQL
SELECT countrylanguage.Language, countrylanguage.Percentage, country.Population * (countrylanguage.Percentage / 100) AS Speakers
FROM countrylanguage
JOIN country ON countrylanguage.CountryCode = country.Code
WHERE country.Name = 'China'
ORDER BY countrylanguage.Percentage DESC;
```
| Language  | Percentage | Speakers         |
|-----------|------------|------------------|
| Chinese   |       92.0 | 1175353360.00000 |
| Zhuang    |        1.4 |   17885812.00000 |
| Mantšu    |        0.9 |   11498022.00000 |
| Hui       |        0.8 |   10220464.00000 |
| Miao      |        0.7 |    8942906.00000 |
| Uighur    |        0.6 |    7665348.00000 |
| Yi        |        0.6 |    7665348.00000 |
| Tujia     |        0.5 |    6387790.00000 |
| Mongolian |        0.4 |    5110232.00000 |
| Tibetan   |        0.4 |    5110232.00000 |
| Dong      |        0.2 |    2555116.00000 |
| Puyi      |        0.2 |    2555116.00000 |


## 12. Languages Spoken in the United States with Percentages
List languages spoken in the United States along with the percentage of speakers for each.
```SQL
SELECT countrylanguage.Language, countrylanguage.Percentage, country.Population * (countrylanguage.Percentage / 100) AS Speakers
FROM countrylanguage
JOIN country ON countrylanguage.CountryCode = country.Code
WHERE country.Name = 'United States'
ORDER BY countrylanguage.Percentage DESC;
```
| Language   | Percentage | Speakers        |
|------------|------------|-----------------|
| English    |       86.2 | 239943734.00000 |
| Spanish    |        7.5 |  20876775.00000 |
| French     |        0.7 |   1948499.00000 |
| German     |        0.7 |   1948499.00000 |
| Chinese    |        0.6 |   1670142.00000 |
| Italian    |        0.6 |   1670142.00000 |
| Tagalog    |        0.4 |   1113428.00000 |
| Korean     |        0.3 |    835071.00000 |
| Polish     |        0.3 |    835071.00000 |
| Japanese   |        0.2 |    556714.00000 |
| Portuguese |        0.2 |    556714.00000 |
| Vietnamese |        0.2 |    556714.00000 |

## 13. Languages Spoken in the United Kingdom with Percentages
Provide a list of languages spoken in the United Kingdom, along with the percentage of speakers for each.
```SQL
SELECT countrylanguage.Language, countrylanguage.Percentage, country.Population * (countrylanguage.Percentage / 100) AS Speakers
FROM countrylanguage
JOIN country ON countrylanguage.CountryCode = country.Code
WHERE country.Name = 'United Kingdom'
ORDER BY countrylanguage.Percentage DESC;
```
| Language | Percentage | Speakers       |
|----------|------------|----------------|
| English  |       97.3 | 58013568.20000 |
| Kymri    |        0.9 |   536610.60000 |
| Gaeli    |        0.1 |    59623.40000 |

## 14. Language in Each Region
For each region, identify the most spoken language and the percentage of the population that speaks it.


## 15. Language Percentages in a Country
Is the sum of language percentages spoken in a country equal to 100? Why?
```SQL
SELECT country.Code AS Country, SUM(countrylanguage.Percentage) AS TotalLanguagePercentage 
FROM country 
JOIN countrylanguage ON country.Code = countrylanguage.CountryCode 
GROUP BY country.Code 
ORDER BY TotalLanguagePercentage DESC;
```

| Country | TotalLanguagePercentage |
|---------|-------------------------|
| NLD     |                   101.0 |
| WSM     |                   100.1 |
| BHS     |                   100.0 |
| BMU     |                   100.0 |
| BTN     |                   100.0 |
| CHL     |                   100.0 |
| CPV     |                   100.0 |
| CRI     |                   100.0 |
| CUB     |                   100.0 |
| DMA     |                   100.0 |
| DOM     |                   100.0 |
| DZA     |                   100.0 |
| ECU     |                   100.0 |
| ESH     |                   100.0 |
| FRO     |                   100.0 |
| GRD     |                   100.0 |
| GRL     |                   100.0 |
| GUY     |                   100.0 |
| HTI     |                   100.0 |
| IRL     |                   100.0 |
| JPN     |                   100.0 |
| KNA     |                   100.0 |
| KOR     |                   100.0 |
| LCA     |                   100.0 |
| LSO     |                   100.0 |
| MDV     |                   100.0 |
| POL     |                   100.0 |
| PRK     |                   100.0 |
| PSE     |                   100.0 |
| RWA     |                   100.0 |
| SLV     |                   100.0 |
| SMR     |                   100.0 |
| TUV     |                   100.0 |


# World Map
![world map](IMAGE/World_map.png)

For creating this world map graphic, I used:

- **Data Source:** MySQL database
- **Tables Used:**
  - 'country'
  - 'countrylanguage'
  - 'city'
- **Python Libraries:**
  - Pandas
  - SQLAlchemy
  - Plotly Express
  - Geopandas

To view the code used for generating this map, [click here](SCRIPT/main.py).

USE classicmodels;

INSERT classicmodels_v2.productlines SELECT * FROM classicmodels.productlines;
INSERT classicmodels_v2.products SELECT * FROM classicmodels.products;

UPDATE classicmodels_v2.offices SET country = "United Kingdom" WHERE country = "UK";
UPDATE classicmodels_v2.offices SET country = "United States" WHERE country = "USA";

INSERT INTO classicmodels_v2.offices (
    officeCode,
    city,
    phone,
    addressLine1,
    addressLine2,
    state,
    country,
    postalCode,
    territory
)
SELECT
    officeCode,
    city,
    phone,
    addressLine1,
    addressLine2,
    state,
    world.country.code,
    postalCode,
    territory
FROM classicmodels.offices
JOIN world.country ON classicmodels.offices.country = world.country.Name;

-- SELECT country FROM classicmodels_v2.offices;

-- INSERT classicmodels_v2.employees SELECT * FROM classicmodels.employees;

UPDATE classicmodels_v2.customers SET country = "United Kingdom" WHERE country = "UK";
UPDATE classicmodels_v2.customers SET country = "United States" WHERE country = "USA";
UPDATE classicmodels_v2.customers SET country = "Russian Federation" WHERE country LIKE "%Russia%";
/* Update Norwway which appears duplicate with and without spaces when:
select country, count(country) from customers group by country order by country;*/
UPDATE classicmodels_v2.customers SET country = "Norway" WHERE country LIKE "%Norway%";

INSERT classicmodels_v2.customers SELECT * FROM classicmodels.customers;

ALTER TABLE customers ADD language char(2) NOT NULL;

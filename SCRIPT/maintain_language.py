import mysql.connector
import csv
import pandas as pd

csv_file_path = 'language-codes_csv.csv'
with open(csv_file_path, 'r') as csv_file:
    csv_reader = csv.reader(csv_file)

conn = mysql.connector.connect(
    host='localhost',
    user='narayan23',
    password='coucou123',
    auth_plugin='mysql_native_password',
    database='world'
)

cursor = conn.cursor()

cursor.execute("""
    select t.code as CountryCode, t.language as language 
from(
    select c.code, 
        cl.language, 
        cl.percentage,
        ROW_NUMBER() OVER (PARTITION BY c.code ORDER BY cl.percentage DESC) AS rn
    from world.country as c 
    join world.countrylanguage as cl on cl.countrycode = c.code 
    where cl.isOfficial = 'T'
) as t
where t.rn = 1;
""")

result = cursor.fetchall()

# Create a DataFrame from the SQL query result
query_df = pd.DataFrame(result, columns=['CountryCode', 'Language'])

# Create a DataFrame from the CSV file, skipping the first row
csv_df = pd.read_csv(csv_file_path, skiprows=1, names=['alpha2', 'English'])

# Merge the two DataFrames on the 'Language' and 'alpha2' columns
merged_df = pd.merge(query_df, csv_df, left_on='Language', right_on='English', how='inner')

# Add language code 'ph' for 'Pilipino' where it's missing
merged_df.loc[merged_df['Language'] == 'Pilipino', 'alpha2'] = 'ph'

# Drop unnecessary columns
merged_df.drop(['English'], axis=1, inplace=True)


# Print or use the merged DataFrame as needed
print(merged_df)


cursor.close()
conn.close()

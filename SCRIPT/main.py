import pandas as pd
from sqlalchemy import create_engine
import plotly.express as px
import geopandas as gpd

# MySQL database connection parameters
connection_params = {
    'host': 'localhost',
    'user': 'narayan23',
    'password': 'coucou123',
    'auth_plugin': 'mysql_native_password',
    'database': 'world'
}

# Create an SQLAlchemy engine
engine = create_engine(f"mysql+mysqlconnector://{connection_params['user']}:{connection_params['password']}@{connection_params['host']}/{connection_params['database']}")

def read_table_query(engine, table_name):
    query = f"SELECT * FROM {table_name}"
    return pd.read_sql(query, engine)

# Read tables into DataFrames
country_df = read_table_query(engine, 'country')
countrylanguage_df = read_table_query(engine, 'countrylanguage')
city_df = read_table_query(engine, 'city')

# Load world GeoJSON data
world_geojson = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))

# Merge GeoDataFrame with the country DataFrame on the appropriate columns (e.g., 'iso_a3' and 'Code')
merged_df = pd.merge(world_geojson, country_df, left_on='iso_a3', right_on='Code', how='left')

# Create the choropleth map using Plotly Express
fig = px.choropleth(
    merged_df,
    geojson=merged_df.geometry,
    locations=merged_df.index,
    color="Region",
    projection="natural earth",
    title="World Map with Region Colors",
    hover_name="name",
    labels={"Region": "Region"},
)

fig.update_layout(
    height=1200,
    width=1900
)

# Show the map
fig.show()
